# Split Landing Page
The inspiration for this design was from <a href="https://codepen.io/bradtraversy/pen/dJzzdB">Traversy Media</a>. Features the use of: 
<ul><li>HTML</li> <li>CSS</li> <li>JavaScript</li></ul>

Finished the website design on March 15, 2018.

<p>Screenshot of website design (featured on <a href="https://dribbble.com/shots/4354342-Split-Landing-Page-Design">Dribbble</a>):</p>
<img src="https://cdn.dribbble.com/users/2010882/screenshots/4354342/edit3.png"></img>
